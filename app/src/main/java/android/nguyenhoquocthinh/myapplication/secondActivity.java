package android.nguyenhoquocthinh.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Nguyen Ho Quoc Thinh on 03/13/2018.
 */

public class secondActivity extends AppCompatActivity {

    private TextView txtInfo;
    private Button btnExit;
    private user data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        txtInfo = (TextView)findViewById(R.id.txtInfo);
        btnExit = (Button) findViewById(R.id.btnExit);

        Intent intent = getIntent();
        data = (user)intent.getSerializableExtra("data");

        String y = "";
        for(int i = 0; i<data.getPassword().length(); i++){
            y = y + "*";
        }

        String x = "Your information registered\nUsername: "+data.getUsername()+"\nPassword: "+y+"\nBirthdate: "
                +data.getBirthdate()+"\nGender: "+data.getGender()+"\nHobbies: "+data.getHobbies();

        txtInfo.setText(x);

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
