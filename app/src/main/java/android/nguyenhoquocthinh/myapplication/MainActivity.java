package android.nguyenhoquocthinh.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtRetype;
    private EditText edtBirthdate;
    private RadioGroup radioGroupGender;
    private RadioButton radioButtonMale;
    private RadioButton radioButtonFemale;
    private CheckBox checkBoxTennis;
    private CheckBox checkBoxFutbal;
    private CheckBox checkBoxOthers;
    private Button btnReset;
    private Button btnSignUp;
    private int index = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtUsername = (EditText)findViewById(R.id.edtUsername);
        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtRetype = (EditText)findViewById(R.id.edtRetype);
        edtBirthdate = (EditText)findViewById(R.id.edtBirthdate);
        radioGroupGender = (RadioGroup) findViewById(R.id.radioGroupGender);
        radioButtonMale = (RadioButton) findViewById(R.id.radioButtonMale);
        radioButtonFemale = (RadioButton)findViewById(R.id.radioButtonFemale);
        checkBoxFutbal = (CheckBox)findViewById(R.id.checkBoxFutbal);
        checkBoxTennis = (CheckBox)findViewById(R.id.checkBoxTennis);
        checkBoxOthers = (CheckBox)findViewById(R.id.checkBoxOthers);
        btnReset = (Button)findViewById(R.id.btnReset);
        btnSignUp = (Button)findViewById(R.id.btnSignUp);

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtUsername.setText("");
                edtPassword.setText("");
                edtRetype.setText("");
                edtBirthdate.setText("");
                radioGroupGender.clearCheck();
                checkBoxTennis.setChecked(false);
                checkBoxFutbal.setChecked(false);
                checkBoxOthers.setChecked(false);
            }
        });

        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                index = i;
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = edtUsername.getText().toString();
                String password= edtPassword.getText().toString();
                String retype = edtRetype.getText().toString();
                if(!retype.equals(password)){
                    Toast.makeText(MainActivity.this, "MẬT KHẨU KHÔNG KHỚP", Toast.LENGTH_SHORT).show();
                    edtRetype.setText("");
                    return;
                }
                String birthdate = edtBirthdate.getText().toString();
                if(!CheckBirthdate(birthdate)){
                    Toast.makeText(MainActivity.this, "BIRTHDATE KHÔNG ĐÚNG ĐỊNH DẠNG\nVUI LÒNG NHẬP LẠI", Toast.LENGTH_SHORT).show();
                    return;
                }
                String gender;
                if(index == 0)
                    gender = "Male";
                else
                    gender = "Female";
                String hobbies = null;
                if(checkBoxTennis.isChecked())
                    hobbies = "Tennis";
                if(checkBoxFutbal.isChecked())
                    hobbies = hobbies + ", Futbal";
                if(checkBoxOthers.isChecked())
                    hobbies = hobbies + ", Others";
                if(username.length()==0||password.length()==0||retype.length()==0||birthdate.length()==0||gender.length()==0||hobbies.length()==0){
                    Toast.makeText(MainActivity.this, "KHÔNG ĐỂ TRỐNG DỮ LIỆU", Toast.LENGTH_SHORT).show();
                    return;
                }
                user data = new user(username, password, birthdate, gender, hobbies);
                Intent intent = new Intent(MainActivity.this, secondActivity.class);
                intent.putExtra("data", data);
                startActivity(intent);
            }
        });


    }

    private boolean CheckBirthdate(String birthdate) {
        if(birthdate.charAt(2) != '/' || birthdate.charAt(5) != '/')
            return false;
        if(birthdate.substring(6).length()!=4)
            return false;
        return true;
    }
}
