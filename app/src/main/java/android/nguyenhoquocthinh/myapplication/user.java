package android.nguyenhoquocthinh.myapplication;

import java.io.Serializable;

/**
 * Created by Nguyen Ho Quoc Thinh on 03/13/2018.
 */

public class user implements Serializable {
    private String username;
    private String password;
    private String birthdate;
    private String gender;
    private String hobbies;

    public user(String username, String password, String birthdate, String gender, String hobbies) {
        this.username = username;
        this.password = password;
        this.birthdate = birthdate;
        this.gender = gender;
        this.hobbies = hobbies;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getGender() {
        return gender;
    }

    public String getHobbies() {
        return hobbies;
    }
}
